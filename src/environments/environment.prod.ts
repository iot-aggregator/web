export const environment = {
  production: true,
  grafanaUrl: window["env"]["grafanaUrl"] || "https://pl.wikipedia.org/wiki/",
  integrationUrl: window["env"]["integrationUrl"] || "http://localhost:8085",
  bucketName: window["env"]["bucketName"] || "photos",
  customBaseHref: window["env"]["customBaseHref"] || "/"
};
