import {Component, OnInit} from '@angular/core';
import {AppConfigService} from "../../common/app-config.service";

@Component({
  selector: 'app-iframe-agent',
  templateUrl: './iframe-agent.component.html',
  styleUrls: ['./iframe-agent.component.css']
})
export class IframeAgentComponent implements OnInit {

  grafanaUrl: string;

  constructor(private appConfig: AppConfigService) {
  }

  ngOnInit(): void {
    this.grafanaUrl = this.appConfig.getGrafanaUrl();
  }

  getHeight() {
    return (document.defaultView.outerHeight * 0.8).toString();
  }
}
