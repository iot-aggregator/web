import {Component, OnInit} from '@angular/core';
import {S3bucketsService} from "../../common/s3bucket/s3buckets.service";
import {GetBucketItemsRequest} from "../../common/s3bucket/s3bucket.dto";
import {PhotoGallery} from "../../common/entities";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  private readonly RIGHT_CLICK: number = 1;

  continuationTokens: string[];
  currentPage: number;
  currentPageGallery: PhotoGallery;
  chosenUrl: string;
  isSelectedOpen: boolean;
  prefix: Prefix = new Prefix();
  loaded: boolean = false;

  constructor(private s3bucketsService: S3bucketsService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    this.s3bucketsService.getPageOfPhotos({size: 12} as GetBucketItemsRequest)
      .subscribe(gallery => {
        this.currentPage = 0;
        this.currentPageGallery = gallery;
        this.chosenUrl = gallery.photos[0].source;
        this.continuationTokens = [null, gallery.continuationToken];
        this.loaded = true;
      });
  }

  goToPage(page: number) {
    this.currentPage = page;
    this.s3bucketsService.getPageOfPhotos({
      size: 12,
      continuationToken: this.continuationTokens[page],
      prefix: this.prefix?.use ? this.prefix.toString() : null
    } as GetBucketItemsRequest)
      .subscribe(gallery => {
        this.currentPageGallery = gallery;
        if (this.continuationTokens.length <= page + 1) {
          this.continuationTokens = [...this.continuationTokens, gallery.continuationToken];
        }
      });
  }

  chosePhoto(src: string): void {
    this.chosenUrl = src;
    this.openModal();
  }

  openInNewTab(src: string, $event: MouseEvent): void {
    if ($event.button === this.RIGHT_CLICK) {
      const image_window = window.open("", "_blank")
      image_window.document.write(`
      <html lang="">
        <head>
        <title></title></head>
        <body>
          <img src="${src}" alt="Example">
        </body>
      </html>
`);
    }
  }

  openModal() {
    this.isSelectedOpen = true;
    document.getElementById('imgModal').style.display = "block";
  }

  closeModal() {
    this.isSelectedOpen = false;
    document.getElementById('imgModal').style.display = "none";
  }

  get isLastPage(): boolean {
    return this.currentPageGallery.continuationToken == null;
  }

  get isFirstPage(): boolean {
    return this.currentPage <= 0
  }

  onPrefixChange() {
    this.s3bucketsService.getPageOfPhotos({
        size: 12,
        prefix: this.prefix?.use ? this.prefix.toString() : null
      } as GetBucketItemsRequest)
      .subscribe(gallery => {
        this.currentPage = 0;
        this.currentPageGallery = gallery;
        this.chosenUrl = gallery.photos[0]?.source;
        this.continuationTokens = [null, gallery.continuationToken];
      });
  }

  isNullOrWhitespace(code: string) {
    return code === null || code === undefined || code.trim() === "";
  }

  isNullOrZero(code: number) {
    return code === null || code === undefined || code <= 0;
  }
}

class Prefix {
  rpiId: string;
  year: number;
  month: number;
  day: number;
  use: boolean;

  toString(): string {
    let prefix = "";
    if (this.rpiId !== null && this.rpiId !== undefined) {
      prefix += this.rpiId
    }
    if (this.year !== null && this.year !== undefined) {
      prefix += `/${this.year}`
    }
    else {
      return prefix;
    }
    if (this.month !== null && this.month !== undefined) {
      prefix += `/${this.month}`
    }
    else {
      return prefix;
    }
    if (this.day !== null && this.day !== undefined) {
      prefix += `/${this.day}`
    }
    return prefix;
  }
}

