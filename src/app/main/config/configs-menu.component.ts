import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {EditRaspberryComponent} from "./edit-raspberry/edit-raspberry.component";
import {filter} from "rxjs/operators";
import {RpiDeviceRepresentation} from "../../common/entities";
import {RaspberriesService} from "../../common/rpis/raspberries.service";

@Component({
  selector: 'app-config',
  templateUrl: './configs-menu.component.html',
  styleUrls: ['./configs-menu.component.css']
})
export class ConfigsMenuComponent implements OnInit {

  raspberries: RpiDeviceRepresentation[];

  constructor(private rpiService: RaspberriesService,
              private dialog: MatDialog) {
    this.raspberries = new Array<RpiDeviceRepresentation>()
  }

  ngOnInit(): void {
    this.rpiService.getAllRaspberries().subscribe(rpis => this.raspberries = rpis);
  }

  removeRpiDevice(id: string): void {
    this.rpiService.deleteRaspberry(id).subscribe(() => this.rpiService.getAllRaspberries().subscribe(rpis => this.raspberries = rpis));
  }

  addRpiDevice(): void {
    const dialogRef = this.dialog.open(EditRaspberryComponent, {
      data: undefined
    });
    dialogRef.afterClosed()
      .pipe(filter(result => result !== undefined))
      .subscribe(result => this.rpiService.addRaspberry(result)
        .subscribe(() => this.rpiService.getAllRaspberries()
          .subscribe(rpis => this.raspberries = rpis)));
  }

  editRpi(rpi: RpiDeviceRepresentation): void {
    const dialogRef = this.dialog.open(EditRaspberryComponent, {
      data: rpi
    });
    dialogRef.afterClosed()
      .pipe(filter(result => result !== undefined))
      .subscribe(result => this.rpiService.editRaspberry(result)
        .subscribe(() => this.rpiService.getAllRaspberries()
          .subscribe(rpis => this.raspberries = rpis)));
  }

}
