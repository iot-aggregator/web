import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EditRaspberryComponent} from './edit-raspberry.component';

describe('EditRaspberryComponent', () => {
  let component: EditRaspberryComponent;
  let fixture: ComponentFixture<EditRaspberryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EditRaspberryComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRaspberryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
