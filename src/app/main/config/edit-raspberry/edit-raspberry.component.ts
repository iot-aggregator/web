import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CreateRpiDeviceDto, UpdateRpiDeviceDto} from "../../../common/rpis/raspberry.dto";
import {RpiDeviceRepresentation} from "../../../common/entities";

@Component({
  selector: 'app-edit-raspberry',
  templateUrl: './edit-raspberry.component.html',
  styleUrls: ['./edit-raspberry.component.css']
})
export class EditRaspberryComponent implements OnInit {
  form: FormGroup;
  isNew: boolean;
  current: RpiDeviceRepresentation;

  constructor(private dialogRef: MatDialogRef<EditRaspberryComponent>,
              private fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) private data: RpiDeviceRepresentation) {
    this.isNew = !data;
    this.createForm(data)
  }

  ngOnInit(): void {
  }

  createForm(data: RpiDeviceRepresentation) {
    this.form = this.fb.group({
        name: [null, [Validators.required], []],
        tube: [null, [Validators.required], []]
      },
      {
        updateOn: 'change'
      });

    if (!!data) {
      this.form.controls['name'].setValue(data.name);
      this.form.controls['tube'].setValue(data.tube);
    }
  }

  isEmpty(control: string): boolean {
    return this.form.controls[control].hasError('required')
  }

  onCancelClick(): void {
    this.dialogRef.close(undefined);
  }

  onOkClick(): void {
    if (this.isNew) {
      this.dialogRef.close({
        name: this.form.controls['name'].value,
        tube: this.form.controls['tube'].value
      } as CreateRpiDeviceDto);
    } else {
      this.dialogRef.close({
        rpiId: this.data.id,
        name: this.form.controls['name'].value,
        tube: this.form.controls['tube'].value
      } as UpdateRpiDeviceDto)
    }
  }

  anyGotError(): boolean {
    console.log(this.form.invalid)
    return this.form.invalid
  }
}
