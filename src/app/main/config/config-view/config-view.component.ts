import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Controller, Device, DeviceType, RpiDevice, Sensor} from "../../../common/entities";
import {MatDialog} from "@angular/material/dialog";
import {EditDeviceComponent} from "./edit-device/edit-device.component";
import {filter} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {RaspberriesService} from "../../../common/rpis/raspberries.service";
import {SensorsService} from "../../../common/sensors/sensors.service";
import {ControllersService} from "../../../common/controller/controllers.service";

@Component({
  selector: 'app-config-view',
  templateUrl: './config-view.component.html',
  styleUrls: ['./config-view.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ConfigViewComponent implements OnInit {
  rpiId: string;
  rpiDevice: RpiDevice;

  controllersDataSource: MatTableDataSource<ControllerDescription>;
  sensorsDataSource: MatTableDataSource<SensorDescription>;
  devicesDefaultColumns = ['pin', 'status', 'name', 'edit', 'delete']

  @ViewChild('controllerPaginator', {static: false}) controllerPaginator: MatPaginator;
  @ViewChild('sensorPaginator', {static: false}) sensorPaginator: MatPaginator;

  constructor(private route: ActivatedRoute,
              private dialog: MatDialog,
              private rpiService: RaspberriesService,
              private sensorsService: SensorsService,
              private controllersService: ControllersService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.rpiId = params['id'];
      this.updateRpiDevice();
    });
  }

  isController(device: any): boolean {
    return device instanceof Controller;
  }

  editDevice(device: Device): void {
    const dialogRef = this.dialog.open(EditDeviceComponent,
      {
        data:
          {
            rpiId: this.rpiId,
            device: device
          },
      });
    dialogRef.afterClosed()
      .pipe(filter(result => result !== undefined))
      .subscribe(result => {
        if (result[0] === DeviceType.CONTROLLER) {
          this.controllersService.editController(result[1]).subscribe(() => this.updateRpiDevice());
        }
        if (result[0] === DeviceType.SENSOR) {
          this.sensorsService.editSensor(result[1]).subscribe(() => this.updateRpiDevice());
        }
      })
  }

  addDevice(): void {
    const dialogRef = this.dialog.open(EditDeviceComponent,
      {
        data: {
          rpiId: this.rpiId,
          device: undefined
        }
      });
    dialogRef.afterClosed()
      .pipe(filter(result => result !== undefined))
      .subscribe(result => {
        if (result[0] === DeviceType.CONTROLLER) {
          this.controllersService.createController(result[1]).subscribe(() => this.updateRpiDevice());
        }
        if (result[0] === DeviceType.SENSOR) {
          this.sensorsService.createSensor(result[1]).subscribe(() => this.updateRpiDevice());
        }
      })
  }

  removeSensor(device: Device): void {
    this.sensorsService.deleteSensor(device.id, this.rpiId).subscribe(() => this.updateRpiDevice())
  }

  removeController(device: Device): void {
    this.controllersService.deleteController(device.id, this.rpiId).subscribe(() => this.updateRpiDevice())
  }

  get controllers(): Controller[] {
    return this.rpiDevice?.config.controllers ?? [];
  }

  get sensors(): Sensor[] {
    return this.rpiDevice?.config.sensors ?? [];
  }

  private updateRpiDevice(): void {
    this.rpiService.getRaspberryConfig(this.rpiId).subscribe(rpi => {
      this.rpiDevice = rpi;
      this.sensorsDataSource = new MatTableDataSource<SensorDescription>(this.sensors.map(sensor => {
        return {
          device: sensor,
          expanded: false
        }
      }));
      this.sensorsDataSource.paginator = this.sensorPaginator;
      this.controllersDataSource = new MatTableDataSource<ControllerDescription>(this.controllers.map(controller => {
        return {
          device: controller,
          expanded: false
        }
      }))
      this.controllersDataSource.paginator = this.controllerPaginator;
    });
  }
}

interface ControllerDescription {
  device: Controller;
  expanded: boolean;
}

interface SensorDescription {
  device: Sensor;
  expanded: boolean;
}
