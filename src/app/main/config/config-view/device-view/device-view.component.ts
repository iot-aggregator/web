import {Component, Input, OnInit} from '@angular/core';
import {Controller, Device, DeviceType, Sensor} from "../../../../common/entities";

@Component({
  selector: 'app-device-view',
  templateUrl: './device-view.component.html',
  styleUrls: ['./device-view.component.css']
})
export class DeviceViewComponent implements OnInit {
  @Input() device: Device;

  constructor() {
  }

  ngOnInit(): void {
  }

  get isController(): boolean {
    return this.device.type === DeviceType.CONTROLLER;
  }

  get isSensor(): boolean {
    return this.device.type === DeviceType.SENSOR;
  }

  get controller(): Controller {
    return Object.assign(new Controller(), this.device);
  }

  get sensor(): Sensor {
    return Object.assign(new Sensor(), this.device);
  }
}
