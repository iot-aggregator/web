import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Controller, Device, DeviceType, Sensor, SensorType, Status} from "../../../../common/entities";
import {CreateControllerDto, UpdateControllerDto} from "../../../../common/controller/controller.dto";
import {CreateSensorDto, UpdateSensorDto} from "../../../../common/sensors/sensor.dto";
import {PinPattern} from "../../../../common/pin-pattern";


@Component({
  selector: 'app-add-device',
  templateUrl: './edit-device.component.html',
  styleUrls: ['./edit-device.component.css']
})
export class EditDeviceComponent implements OnInit {
  deviceType: DeviceType;
  form: FormGroup;
  sensorType = Object.keys(SensorType).map(key => SensorType[key]);
  pattern: string

  constructor(private dialogRef: MatDialogRef<EditDeviceComponent>,
              private fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) private data: { rpiId: string, device: Device }) {
    if (!this.isNew) {
      if (data.device instanceof Controller) {
        this.createControllerForm(data.device);
        this.pattern = PinPattern.CONTROLLER;
      }
      if (data.device instanceof Sensor) {
        this.createSensorForm(data.device);
        this.pattern = PinPattern.SENSOR;
      }
    } else {
      this.deviceType = undefined;
    }
  }

  ngOnInit(): void {
  }

  createSensorForm(device: Sensor): void {
    this.pattern = PinPattern.SENSOR;
    this.form = this.fb.group({
        pinName: [null, [Validators.required], []],
        status: [null, [Validators.required], []],
        name: [null, [Validators.required], []],
        description: [null, [Validators.required], []],
        measurementType: [null, [Validators.required], []],
        sensorType: [null, [Validators.required], []],
        coefficientA: [null, [Validators.required], []],
        coefficientB: [null, [Validators.required], []]
      },
      {
        updateOn: 'change'
      });
    this.form.controls['status'].setValue(true);

    if (!!device) {
      this.setCommonForm(device);
      this.form.controls['measurementType'].setValue(device.measurementType);
      this.form.controls['sensorType'].setValue(device.sensorType);
      this.form.controls['coefficientA'].setValue(device.coefficientA);
      this.form.controls['coefficientB'].setValue(device.coefficientB);
    }
    this.deviceType = DeviceType.SENSOR;
  }

  createControllerForm(device: Controller): void {
    this.pattern = PinPattern.CONTROLLER;
    this.form = this.fb.group({
        pinName: [null, [Validators.required], []],
        status: [null, [Validators.required], []],
        name: [null, [Validators.required], []],
        description: [null, [Validators.required], []],
        waitingTimeInSeconds: [null, [Validators.required], []],
        actionTimeInSeconds: [null, [Validators.required], []]
      },
      {
        updateOn: 'change'
      });
    this.form.controls['status'].setValue(true);

    if (!!device) {
      this.setCommonForm(device);
      this.form.controls['waitingTimeInSeconds'].setValue(device.waitingTimeInSeconds);
      this.form.controls['actionTimeInSeconds'].setValue(device.actionTimeInSeconds);
    }

    this.deviceType = DeviceType.CONTROLLER;
  }

  get isNew(): boolean {
    return this.data.device === undefined;
  }

  isEmpty(control: string): boolean {
    return this.form.controls[control].errors !== undefined;
  }

  isController(): boolean {
    return this.deviceType === DeviceType.CONTROLLER;
  }

  isSensor(): boolean {
    return this.deviceType === DeviceType.SENSOR;
  }

  isPinActive(): boolean {
    return this.form.controls['status'].value;
  }

  anyGotError(): boolean {
    console.log(this.form.invalid)
    return this.form.invalid
  }

  onCancelClick(): void {
    this.dialogRef.close(undefined);
  }

  onOkClick(): void {
    if (this.isController()) {
      this.onControllerOkClick();
      return;
    }
    if (this.isSensor()) {
      this.onSensorOkClick();
      return;
    }
  }

  private onControllerOkClick(): void {
    let dto;
    if (this.isNew) {
      dto = {
        rpiId: this.data.rpiId,
        pinName: this.form.controls['pinName'].value,
        status: this.isPinActive() ? Status.ACTIVE : Status.INACTIVE,
        name: this.form.controls['name'].value,
        description: this.form.controls['description'].value,
        waitingTimeInSeconds: this.form.controls['waitingTimeInSeconds'].value,
        actionTimeInSeconds: this.form.controls['actionTimeInSeconds'].value
      } as CreateControllerDto;
    } else {
      dto = {
        id: this.data.device.id,
        rpiId: this.data.rpiId,
        pinName: this.form.controls['pinName'].value,
        status: this.isPinActive() ? Status.ACTIVE : Status.INACTIVE,
        name: this.form.controls['name'].value,
        description: this.form.controls['description'].value,
        waitingTimeInSeconds: this.form.controls['waitingTimeInSeconds'].value,
        actionTimeInSeconds: this.form.controls['actionTimeInSeconds'].value
      } as UpdateControllerDto;
    }
    this.dialogRef.close([DeviceType.CONTROLLER, dto])
  }

  private onSensorOkClick(): void {
    let dto;
    console.log(this.form.controls)
    if (this.isNew) {
      dto = {
        rpiId: this.data.rpiId,
        pinName: this.form.controls['pinName'].value,
        status: this.isPinActive() ? Status.ACTIVE : Status.INACTIVE,
        name: this.form.controls['name'].value,
        description: this.form.controls['description'].value,
        measurementType: this.form.controls['measurementType'].value,
        type: this.form.controls['sensorType'].value,
        coefficientA: this.form.controls['coefficientA'].value,
        coefficientB: this.form.controls['coefficientB'].value,
      } as CreateSensorDto;
    } else {
      dto = {
        id: this.data.device.id,
        rpiId: this.data.rpiId,
        pinName: this.form.controls['pinName'].value,
        status: this.isPinActive() ? Status.ACTIVE : Status.INACTIVE,
        name: this.form.controls['name'].value,
        description: this.form.controls['description'].value,
        measurementType: this.form.controls['measurementType'].value,
        type: this.form.controls['sensorType'].value,
        coefficientA: this.form.controls['coefficientA'].value,
        coefficientB: this.form.controls['coefficientB'].value,
      } as UpdateSensorDto;
    }
    this.dialogRef.close([DeviceType.SENSOR, dto])
  }

  private setCommonForm(data: Device): void {
    this.form.controls['pinName'].setValue(data.pinName);
    this.form.controls['status'].setValue(data.status === Status.ACTIVE);
    this.form.controls['name'].setValue(data.name);
    this.form.controls['description'].setValue(data.description);
  }
}
