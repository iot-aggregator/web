import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AppConfigService} from "../app-config.service";
import {GetBucketItemsRequest, GetBucketItemsRespond} from "./s3bucket.dto";
import {map, share} from "rxjs/operators";
import {Observable} from "rxjs";
import {Photo, PhotoGallery} from "../entities";

@Injectable({
  providedIn: 'root'
})
export class S3bucketsService {

  private readonly url: string;
  private readonly bucketName: string;

  constructor(private http: HttpClient,
              private env: AppConfigService) {
    this.bucketName = env.getBucketName();
    this.url = `${env.getS3ServiceUrl()}/buckets/list/${this.bucketName}`;
  }

  getPageOfPhotos(dto: GetBucketItemsRequest): Observable<PhotoGallery> {
    return this.http.get(this.url + S3bucketsService.getBucketItemsRequestToParams(dto))
      .pipe(
        map(res => res as GetBucketItemsRespond),
        map(respond => {
          const gallery = new PhotoGallery();
          gallery.photos = respond.links.map(link => this.linkToPhoto(link));
          gallery.continuationToken = respond.continuationToken;
          return gallery;
        }),
        share());
  }

  private linkToPhoto(link: string): Photo {
    const photo = new Photo();
    photo.source = link;
    const chunks: string[] = link.split('/');
    const bucketChunkIndex: number = chunks.indexOf(this.bucketName);
    photo.rpiId = chunks[bucketChunkIndex + 1];
    photo.date = new Date(chunks[chunks.length - 2]);
    return photo;
  }

  private static getBucketItemsRequestToParams(dto: GetBucketItemsRequest): string {
    const params =  new Array<string>()
    for (let param of Object.keys(dto)) {
      if (dto[param] !== null && dto[param] !== undefined) {
        params.push(`${param}=${dto[param]}`);
      }
    }
    return '?' + params.join('&');
  }
}
