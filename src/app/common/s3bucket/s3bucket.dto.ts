export interface GetBucketItemsRequest {
  size: number;
  prefix: string;
  afterMarker: string;
  continuationToken: string;
}

export interface GetBucketItemsRespond {
  links: string[],
  continuationToken: string
}
