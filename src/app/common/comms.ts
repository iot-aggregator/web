import {Observable, ObservedValueOf, of, OperatorFunction} from "rxjs";
import {catchError} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";

export function logErrorToSnackBar(snackBar: MatSnackBar): OperatorFunction<unknown, ObservedValueOf<Observable<boolean>>> {
  return catchError((data: HttpErrorResponse) => {
    const errors = data.error.errors
    let message = "";
    for(const err in errors) {
      message += errors[err].join(',');
    }
    snackBar.open(message, 'close')
    return of(false)
  }) as OperatorFunction<unknown, boolean>
}
