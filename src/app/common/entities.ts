export class RpiDeviceRepresentation {
  id: string;
  name: string;
  tube: number;
}

export class RpiDevice {
  id: string;
  name: string;
  tube: number;
  config: RpiConfig;
}

export interface Device {
  rpiId: string;
  pinName: string;
  status: Status;
  id: string;
  name: string;
  description: string;

  get type();
}

export class Sensor implements Device {
  rpiId: string;
  pinName: string;
  status: Status;
  id: string;
  name: string;
  description: string;
  measurementType: string;
  sensorType: SensorType;
  coefficientA: number;
  coefficientB: number;

  get type(): DeviceType {
    return DeviceType.SENSOR;
  }
}

export class Controller implements Device {
  rpiId: string;
  pinName: string;
  status: Status;
  id: string;
  name: string;
  description: string;
  waitingTimeInSeconds: number;
  actionTimeInSeconds: number;

  get type(): DeviceType {
    return DeviceType.CONTROLLER;
  }
}

export class RpiConfig {
  sensors: Sensor[];
  controllers: Controller[];
}

export class Photo {
  source: string;
  date: Date;
  rpiId: string;
}

export class PhotoGallery {
  photos: Photo[];
  continuationToken: string;
}

export enum Status {
  ACTIVE = 'active',
  INACTIVE = 'inactive'
}

export enum SensorType {
  DIGITAL = 'digital',
  ANALOG = 'analog'
}

export enum DeviceType {
  CONTROLLER,
  SENSOR
}
