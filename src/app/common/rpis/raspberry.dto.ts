import {ControllerDto} from "../controller/controller.dto";
import {SensorDto} from "../sensors/sensor.dto";

export interface GetRpiDeviceRepresentationDto {
  id: string;
  name: string;
  tube: number;
}

export interface GetRpiDeviceDto {
  id: string;
  name: string;
  tube: number;
  config: RaspberryConfigurationDto;
}

export interface RaspberryConfigurationDto {
  sensors: SensorDto[];
  controllers: ControllerDto[];
}

export interface CreateRpiDeviceDto {
  name: string;
  tube: number;
}

export interface UpdateRpiDeviceDto {
  rpiId: string;
  name: string;
  tube: number;
}
