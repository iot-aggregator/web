import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {Controller, RpiConfig, RpiDevice, RpiDeviceRepresentation, Sensor} from "../entities";
import {catchError, map, share, tap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {AppConfigService} from "../app-config.service";
import {CreateRpiDeviceDto, GetRpiDeviceDto, GetRpiDeviceRepresentationDto, UpdateRpiDeviceDto} from "./raspberry.dto";
import {ControllerDto} from "../controller/controller.dto";
import {SensorDto} from "../sensors/sensor.dto";
import {MatSnackBar} from "@angular/material/snack-bar";
import {logErrorToSnackBar} from "../comms";

@Injectable({
  providedIn: 'root'
})
export class RaspberriesService {
  private readonly url: string;

  constructor(private http: HttpClient,
              private env: AppConfigService,
              private snackBar: MatSnackBar) {
    this.url = env.getRaspberriesServiceUrl();
  }

  public getAllRaspberries(): Observable<RpiDeviceRepresentation[]> {
    return this.http.get(this.url + "/raspberries")
      .pipe(
        map(response => Object.assign([], response)),
        map(response => response.map(rpi => rpi as GetRpiDeviceRepresentationDto)),
        map(rpiDevices => rpiDevices.map(rpi => Object.assign(new RpiDeviceRepresentation(), {
          id: rpi.id,
          name: rpi.name,
          tube: rpi.tube
        } as RpiDeviceRepresentation))),
        share());
  }

  public getRaspberryConfig(rpiId: string): Observable<RpiDevice> {
    return this.http.get(this.url + `/raspberries/${rpiId}`)
      .pipe(
        map(response => response as GetRpiDeviceDto),
        map(rpiDto => this.rpiDtoToRpiEntity(rpiDto)),
        tap(rpi => console.log(rpi)),
        share()
      )
  }

  public deleteRaspberry(rpiId: string): Observable<boolean> {
    return this.http.delete(this.url + `/raspberries/${rpiId}`)
      .pipe(
        map(() => true),
        tap(() => console.log(`Raspberry ${rpiId} deleted successfully`)),
        catchError(() => of(false)),
        share()
      );
  }

  public editRaspberry(rpi: UpdateRpiDeviceDto): Observable<boolean> {
    return this.http.put(`${this.url}/raspberries/${rpi.rpiId}`, rpi)
      .pipe(
        map(() => true),
        tap(() => console.log(`Raspberry ${rpi.rpiId} edited successfully`)),
        logErrorToSnackBar(this.snackBar),
        share()
      );
  }

  public addRaspberry(rpi: CreateRpiDeviceDto): Observable<boolean> {
    return this.http.post(`${this.url}/raspberries`, rpi)
      .pipe(
        map(() => true),
        tap(() => console.log(`Raspberry ${rpi.name} created successfully`)),
        logErrorToSnackBar(this.snackBar),
        share()
      );
  }

  private rpiDtoToRpiEntity(rpiDto: GetRpiDeviceDto): RpiDevice {
    const rpi = new RpiDevice();
    rpi.id = rpiDto.id;
    rpi.name = rpiDto.name;
    rpi.tube = rpiDto.tube;
    const config = new RpiConfig();
    config.controllers = rpiDto.config.controllers
      .map(c => c as ControllerDto)
      .map(c => Object.assign(new Controller(), {
        id: c.id,
        name: c.name,
        description: c.description,
        pinName: c.pinName,
        status: c.status,
        waitingTimeInSeconds: c.waitingTimeInSeconds,
        actionTimeInSeconds: c.actionTimeInSeconds
      } as Controller));
    config.sensors = rpiDto.config.sensors
      .map(s => s as SensorDto)
      .map(s => Object.assign(new Sensor(), {
        id: s.id,
        name: s.name,
        description: s.description,
        pinName: s.pinName,
        status: s.status,
        sensorType: s.sensorType,
        measurementType: s.measurementType,
        coefficientA: s.coefficientA,
        coefficientB: s.coefficientB
      } as Sensor));
    rpi.config = config;
    return rpi;
  }
}
