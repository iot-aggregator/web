export enum PinPattern {
  CONTROLLER = '(GPIO)\\d+',
  SENSOR = '(ANALOG|GPIO)\\d+'
}
