import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {catchError, map, share, tap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {AppConfigService} from "../app-config.service";
import {CreateSensorDto, UpdateSensorDto} from "./sensor.dto";
import {logErrorToSnackBar} from "../comms";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class SensorsService {

  private readonly url: string;

  constructor(private http: HttpClient,
              private env: AppConfigService,
              private snackBar: MatSnackBar) {
    this.url = env.getSensorsServiceUrl();
  }

  public createSensor(sensorDto: CreateSensorDto): Observable<boolean> {
    return this.http.post(`${this.url}/raspberries/${sensorDto.rpiId}/sensors`, sensorDto)
      .pipe(
        map(() => true),
        tap(() => console.log(`Sensor ${sensorDto.name} created successfully`)),
        logErrorToSnackBar(this.snackBar),
        share()
      );
  }

  public editSensor(sensorDto: UpdateSensorDto): Observable<boolean> {
    return this.http.put(`${this.url}/raspberries/${sensorDto.rpiId}/sensors/${sensorDto.id}`, sensorDto)
      .pipe(
        map(() => true),
        tap(() => console.log(`Sensor ${sensorDto.id} edited successfully`)),
        logErrorToSnackBar(this.snackBar),
        share()
      );
  }

  public deleteSensor(deviceId: string, rpiId: string): Observable<boolean> {
    return this.http.delete(`${this.url}/raspberries/${rpiId}/sensors/${deviceId}`)
      .pipe(
        map(() => true),
        tap(() => console.log(`Sensor ${deviceId} on ${rpiId} deleted successfully`)),
        catchError(() => of(false)),
        share()
      );
  }
}
