import {SensorType, Status} from "../entities";

export interface SensorDto {
  id: string;
  pinName: string;
  status: string;
  name: string;
  description: string;
  measurementType: string;
  sensorType: string;
  coefficientA: number;
  coefficientB: number;
}

export interface CreateSensorDto {
  rpiId: string;
  pinName: string;
  status: Status;
  name: string;
  description: string;
  measurementType: string;
  type: SensorType;
  coefficientA: number;
  coefficientB: number;
}

export interface UpdateSensorDto {
  id: string;
  rpiId: string;
  pinName: string;
  status: Status;
  name: string;
  description: string;
  measurementType: string;
  type: SensorType;
  coefficientA: number;
  coefficientB: number;
}
