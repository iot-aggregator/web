import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {catchError, map, share, tap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {AppConfigService} from "../app-config.service";
import {CreateControllerDto, UpdateControllerDto} from "./controller.dto";
import {MatSnackBar} from "@angular/material/snack-bar";
import {logErrorToSnackBar} from "../comms";

@Injectable({
  providedIn: 'root'
})
export class ControllersService {

  private readonly url: string;

  constructor(private http: HttpClient,
              private env: AppConfigService,
              private snackBar: MatSnackBar) {
    this.url = env.getControllersServiceUrl();
  }

  public createController(controllerDto: CreateControllerDto): Observable<boolean> {
    return this.http.post(`${this.url}/raspberries/${controllerDto.rpiId}/device-controllers`, controllerDto)
      .pipe(
        map(() => true),
        tap(() => console.log(`Sensor ${controllerDto.name} created successfully`)),
        logErrorToSnackBar(this.snackBar),
        share()
      );
  }

  public editController(controllerDto: UpdateControllerDto): Observable<boolean> {
    return this.http.put(`${this.url}/raspberries/${controllerDto.rpiId}/device-controllers/${controllerDto.id}`, controllerDto)
      .pipe(
        map(() => true),
        tap(() => console.log(`Controller ${controllerDto.id} edited successfully`)),
        logErrorToSnackBar(this.snackBar),
        share()
      );
  }

  public deleteController(deviceId: string, rpiId: string): Observable<boolean> {
    return this.http.delete(`${this.url}/raspberries/${rpiId}/device-controllers/${deviceId}`)
      .pipe(
        map(() => true),
        tap(() => console.log(`Controller ${deviceId} on ${rpiId} deleted successfully`)),
        catchError(() => of(false)),
        share()
      );
  }
}
