import {Status} from "../entities";

export interface ControllerDto {
  id: string;
  pinName: string;
  status: string;
  name: string;
  description: string;
  waitingTimeInSeconds: number;
  actionTimeInSeconds: number;
}

export interface CreateControllerDto {
  rpiId: string;
  pinName: string;
  status: Status;
  name: string;
  description: string;
  waitingTimeInSeconds: number;
  actionTimeInSeconds: number;
}

export interface UpdateControllerDto {
  id: string;
  rpiId: string;
  pinName: string;
  status: Status;
  name: string;
  description: string;
  waitingTimeInSeconds: number;
  actionTimeInSeconds: number;
}
