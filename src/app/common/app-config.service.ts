import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  constructor() {
  }

  getGrafanaUrl(): string {
    return environment.grafanaUrl;
  }

  getRaspberriesServiceUrl(): string {
    return environment.integrationUrl;
  }

  getControllersServiceUrl(): string {
    return environment.integrationUrl;
  }

  getSensorsServiceUrl(): string {
    return environment.integrationUrl;
  }

  getS3ServiceUrl(): string {
    return environment.integrationUrl;
  }

  getBucketName(): string {
    return environment.bucketName;
  }

  getBaseHref(): string {
    return environment.customBaseHref;
  }
}
