import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {MatToolbarModule} from "@angular/material/toolbar";
import {FooterComponent} from './footer/footer.component';
import {MatTableModule} from "@angular/material/table";
import {MatCardModule} from "@angular/material/card";
import {MatGridListModule} from "@angular/material/grid-list";
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {MatButtonModule} from "@angular/material/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {IframeAgentComponent} from './main/iframe-agent/iframe-agent.component';
import {SafePipe} from "./common/safe.pipe";
import {GalleryComponent} from './main/gallery/gallery.component';
import {ConfigsMenuComponent} from './main/config/configs-menu.component';
import {ConfigViewComponent} from './main/config/config-view/config-view.component';
import {DeviceViewComponent} from './main/config/config-view/device-view/device-view.component';
import {EditDeviceComponent} from './main/config/config-view/edit-device/edit-device.component';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatSelectModule} from "@angular/material/select";
import {EditRaspberryComponent} from './main/config/edit-raspberry/edit-raspberry.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {APP_BASE_HREF} from "@angular/common";

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    FooterComponent,
    IframeAgentComponent,
    SafePipe,
    GalleryComponent,
    ConfigsMenuComponent,
    ConfigViewComponent,
    DeviceViewComponent,
    EditDeviceComponent,
    EditRaspberryComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {path: '', component: IframeAgentComponent},
      {path: 'gallery', component: GalleryComponent, pathMatch: 'full'},
      {path: 'configs', component: ConfigsMenuComponent, pathMatch: 'full'},
      {path: 'configs/:id', component: ConfigViewComponent}
    ], {useHash: true}),
    HttpClientModule,
    MatToolbarModule,
    MatTableModule,
    MatCardModule,
    MatGridListModule,
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    MatButtonModule,
    FormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatButtonToggleModule,
    MatSnackBarModule
  ],
  exports: [RouterModule],
  providers: [{provide: APP_BASE_HREF, useValue: environment.customBaseHref}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
