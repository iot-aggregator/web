import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  contributors = ['RafKulas', 'Olczix', 'krzysztofzajaczkowski'].sort();

  constructor() {
  }

  ngOnInit(): void {
  }

}
