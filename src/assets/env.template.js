(function(window) {
  window.env = window.env || {};
  // Environment variables
  window["env"]["grafanaUrl"] = "${GRAFANA_URL}";
  window["env"]["integrationUrl"] = "${INTEGRATION_URL}";
  window["env"]["bucketName"] = "${BUCKET_NAME}";
  window["env"]["customBaseHref"] = "${CUSTOM_BASE_HREF}";
})(this);
