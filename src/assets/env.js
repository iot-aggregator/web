(function(window) {
  window["env"] = window["env"] || {};
  window["env"]["grafanaUrl"] = "https://pl.wikipedia.org/wiki/Wikipedia:Strona_g%C5%82%C3%B3wna";
  window["env"]["integrationUrl"] = "http://localhost:8085";
  window["env"]["bucketName"] = "photos";
  window["env"]["customBaseHref"] = "/";
})(this);
