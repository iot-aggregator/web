#### STAGE 1
FROM node:lts-alpine as builder
WORKDIR /app
RUN npm install -g @angular/cli
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN ng build --configuration production --base-href REVERSE_PROXY_BASE_HREF --output-path=/dist

#### STAGE 2
FROM nginx:alpine
COPY --from=builder /dist /usr/share/nginx/html

COPY --from=builder /app/.docker/nginx.conf /etc/nginx/nginx.conf

COPY ./custom-entrypoint.sh /custom-entrypoint.sh

ENTRYPOINT ["sh", "/custom-entrypoint.sh"]
