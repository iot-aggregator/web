#!/bin/sh

sed -i "s|<base href=\"REVERSE_PROXY_BASE_HREF\">|<base href=\"${REVERSE_PROXY_BASE_HREF}\">|g" /usr/share/nginx/html/index.html

envsubst < /usr/share/nginx/html/assets/env.template.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'
